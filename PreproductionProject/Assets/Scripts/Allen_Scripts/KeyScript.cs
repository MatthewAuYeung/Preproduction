﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyScript : MonoBehaviour
{
    // Start is called before the first frame update
    public enum KeyColor
    {
        Red,
        Green,
        Normal
    }

    public KeyColor keyColor = KeyColor.Normal;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKey(KeyCode.Space))
        //{
        //    Destroy(gameObject);
        //}
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Collided with " + other.gameObject.name);
        if (other.gameObject.CompareTag("PlayerTag"))
        {
            other.gameObject.GetComponentInParent<NewPlayerScript>().AddKey(keyColor);
            Destroy(gameObject);
        }
    }
}
